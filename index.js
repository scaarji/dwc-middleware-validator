'use strict';
var util = require('util');

var validator = require('is-my-json-valid');

module.exports = function(rules, formats, schemas) {
    var validators = Object.keys(rules).reduce(function(validators, op) {
        validators.set(op, validator(rules[op], {
            formats: formats,
            schemas: schemas,
            verbose: true
        }));

        return validators;
    }, new Map());
    var createErrorMessage = function(err) {
        var fieldName = err.field.replace(/^data\./, '');
        if (err.message === 'is required') {
            return util.format('Field [%s] is required', fieldName);
        }

        return util.format('Invalid value [%s] in field [%s], %s',
                           err.value, fieldName, err.message);
    };
    var createErrorObject = function(validationErrors) {
        return {
            name: 'ValidationError',
            message: validationErrors.map(createErrorMessage).join('. ')
        };
    };

    return {
        pre: function(message, callback) {
            if (!validators.has(message.op)) {
                return process.nextTick(function() {
                    callback(null, message);
                });
            }

            var validate = validators.get(message.op);
            var isRequestValid = validate(message.params);
            if (isRequestValid) {
                return process.nextTick(function() {
                    callback(null, message);
                });
            }

            var error = createErrorObject(validate.errors);

            process.nextTick(function() {
                callback(error);
            });
        }
    };
};
