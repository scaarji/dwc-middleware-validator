/* global describe, it, beforeEach */
'use strict';
var should = require('should');
var createValidationMiddleware = require('../index');

describe('Validation Middleware', function() {
    beforeEach(function() {
        this.middleware = createValidationMiddleware({
            info: {
                type: 'object',
                properties: {
                    foo: {
                        type: 'string'
                    }
                },
                required: ['foo']
            }
        });
    });

    it('should contain pre hook', function() {
        this.middleware.should.have.property('pre').which.is.type('function');
    });

    it('should allow request to ops that don\'t have rules', function(done) {
        var message = {
            op: 'get'
        };
        this.middleware.pre(message, function(err, processedMessage) {
            should.not.exist(err);
            message.should.eql(processedMessage);

            done();
        });
    });

    it('should allow requests to op that have rules with valid parameters', function(done) {
        var message = {
            op: 'info',
            params: {
                foo: 'bar'
            }
        };

        this.middleware.pre(message, function(err, processedMessage) {
            should.not.exist(err);
            message.should.eql(processedMessage);
            done();
        });
    });

    it('should not allow requests to op that have rules with missing parameter', function(done) {
        var message = {
            op: 'info',
            params: {
                bar: 'baz'
            }
        };

        this.middleware.pre(message, function(err, processedMessage) {
            should.exist(err);
            err.name.should.eql('ValidationError');
            err.message.should.eql('Field [foo] is required');

            should.not.exist(processedMessage);
            done();
        });
    });
    it('should not allow requests to op that have rules with invalid parameters', function(done) {
        var message = {
            op: 'info',
            params: {
                foo: 1,
                bar: 'baz'
            }
        };

        this.middleware.pre(message, function(err, processedMessage) {
            should.exist(err);
            err.name.should.eql('ValidationError');
            err.message.should.eql('Invalid value [1] in field [foo], is the wrong type');
            should.not.exist(processedMessage);
            done();
        });
    });
});
